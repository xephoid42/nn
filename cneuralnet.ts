///<reference path='sneuron_layer.ts'/>
///<reference path='cparams.ts'/>

class CNeuralNet {
    private numInputs: number;
    private numOutputs: number;
    private numHiddenLayers: number;
    private neuronsPerHiddenLayer: number;

    private vecLayers: SNeuronLayer[] = [];

    constructor() {
        this.numInputs = CParams.NUM_INPUTS;
        this.numOutputs = CParams.NUM_OUTPUTS;
        this.numHiddenLayers = CParams.NUM_HIDDEN;
        this.neuronsPerHiddenLayer = CParams.NEURONS_PER_HIDDEN_LAYER;

        this.createNet();
    }

    createNet(): void {
        //create the layers of the network
        if (this.numHiddenLayers > 0) {
            // create first hidden layer
            this.vecLayers.push(new SNeuronLayer(this.neuronsPerHiddenLayer, this.numInputs));


            for (var i: number = 0; i<this.numHiddenLayers-1; ++i) {
                this.vecLayers.push(new SNeuronLayer(this.neuronsPerHiddenLayer, this.neuronsPerHiddenLayer));
            }

            // create output layer
            this.vecLayers.push(new SNeuronLayer(this.numOutputs, this.neuronsPerHiddenLayer));
        } else {
            // create output layer
            this.vecLayers.push(new SNeuronLayer(this.numOutputs, this.neuronsPerHiddenLayer));
        }
    }

    getWeights(): number[] {
        var weights: number[] = [];

        // for each layer
        for (var i: number = 0; i<this.numHiddenLayers+1; ++i) {
            // for each neuron
            for (var j: number = 0; j<this.vecLayers[i].numNeurons; ++j) {
                // for each weight
                for (var k: number = 0; k<this.vecLayers[i].neurons[j].numInputs; ++k) {
                    weights.push(this.vecLayers[i].neurons[j].weights[k]);
                }
            }
        }

        return weights;
    }

    putWeights(weights: number[]): void {
        var cWeight: number = 0;
        // for each layer
        for (var i: number = 0; i<this.numHiddenLayers+1; ++i) {
            // for each neuron
            for (var j:number = 0; j < this.vecLayers[i].numNeurons; ++j) {
                // for each weight
                for (var k:number = 0; k < this.vecLayers[i].neurons[j].numInputs; ++k) {
                    this.vecLayers[i].neurons[j].weights[k] = weights[cWeight++];
                }
            }
        }
    }

    getNumberOfWeights(): number {
        var weights: number = 0;

        // for each layer
        for (var i: number = 0; i<this.numHiddenLayers+1; ++i) {
            // for each neuron
            for (var j:number = 0; j < this.vecLayers[i].numNeurons; ++j) {
                // for each weight
                for (var k:number = 0; k < this.vecLayers[i].neurons[j].numInputs; ++k) {
                    weights++;
                }
            }
        }

        return weights;
    }

    update(inputs: number[]): number[] {
        var outputs: number[] = [];
        var cWeight = 0;

        // first check that we have the correct amount of inputs
        if (inputs.length != this.numInputs) {
            // just return an empty vector if incorrect.
            return outputs;
        }

        // For each layer....
        for (var i: number = 0; i<this.numHiddenLayers + 1; ++i) {
            if (i > 0) {
                inputs = outputs;
            }

            outputs = [];
            cWeight = 0;

            // for each neuron sum the (inputs * corresponding weights).Throw
            // the total at our sigmoid function to get the output.
            for (var j: number = 0; j < this.vecLayers[i].numNeurons; ++j) {
                var netInput: number = 0;
                var numInputs: number = this.vecLayers[i].neurons[j].numInputs;

                // for each weight
                for (var k: number = 0; k<numInputs - 1; ++k) {
                    // sum the weights x inputs
                    netInput += this.vecLayers[i].neurons[j].weights[k] * inputs[cWeight++];
                }

                // add in the bias
                netInput += this.vecLayers[i].neurons[j].weights[numInputs-1] * CParams.BIAS;

                // we can store the outputs from each layer as we generate them.
                // The combined activation is first filtered through the sigmoid
                // function
                outputs.push(this.sigmoid(netInput, CParams.ACTIVATION_RESPONSE));

                cWeight = 0;
            }
        }

        return outputs;
    }

    sigmoid(activation: number, response: number): number {
        return (1 / (1 + Math.exp(-activation / response)));
    }
}