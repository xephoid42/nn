class CParams {
    public static BIAS: number                      = -1;
    public static ACTIVATION_RESPONSE: number       = 1.0;

    public static NUM_INPUTS: number                = 7;
    public static NUM_OUTPUTS: number               = 1;
    public static NUM_HIDDEN: number                = 1;
    public static NEURONS_PER_HIDDEN_LAYER: number  = 7;

    public static MAX_PURTURBATION: number          = 0.3;

    public static NUM_COPIES_ELITE: number          = 4;
    public static NUM_ELITE: number                 = 1;

    public static MUTATION_RATE: number             = 0.1;
    public static CROSSOVER_RATE: number            = 0.7;
}