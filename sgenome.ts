class SGenome {
    weights: number[];
    fitness: number;

    constructor(w: number[], f: number) {
        this.weights = w;
        this.fitness = f;
    }
}