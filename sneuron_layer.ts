///<reference path='sneuron.ts'/>

class SNeuronLayer {
    numNeurons: number;
    neurons: SNeuron[] = [];

    constructor(numNeurons: number, numInputsPerNeuron: number) {
        this.numNeurons = numNeurons;
        for (var i: number = 0; i<numNeurons; ++i) {
            this.neurons.push(new SNeuron(numInputsPerNeuron))
        }
    }
}