class SNeuron {
    numInputs: number;

    weights: number[] = [];

    constructor(inputs: number) {
        this.numInputs = inputs + 1; // plus one for the bias

        for (var i: number = 0; i<inputs; ++i) {
            this.weights.push(Math.random() - Math.random());
        }
    }
}