///<reference path='sgenome.ts'/>
///<reference path='cparams.ts'/>

class CGenAlg {
    private popSize: number;
    private mutationRate: number;
    private crossoverRate: number;
    private chromoLength: number;
    private totalFitness: number;
    private generation: number;
    private fittestGenome: number;
    private bestFitness: number;
    private worstFitness: number;
    private averageFitness: number;

    private population: SGenome[] = [];

    constructor(popsize: number, mutRat: number, crossRat: number, numWeights: number) {
        this.popSize = popsize;
        this.mutationRate = mutRat;
        this.crossoverRate = crossRat;
        this.chromoLength = numWeights;
        this.totalFitness = 0;
        this.generation = 0;
        this.fittestGenome = 0;
        this.bestFitness = 0;
        this.worstFitness = 99999999;
        this.averageFitness = 0;

        for (var i: number = 0; i<this.popSize; ++i) {
            this.population.push(new SGenome([], 0));
            for (var j: number = 0; j<this.chromoLength; ++j) {
                this.population[i].weights.push(Math.random() - Math.random());
            }
        }
    }

    private mutate(chromo: number[]): void {
        for (var i: number = 0; i<chromo.length; ++i) {
            if (Math.random() < this.mutationRate) {
                chromo[i] += (Math.random() - Math.random()) * CParams.MAX_PURTURBATION;
            }
        }
    }

    private getChromoRoulette(): SGenome {
        var slice: number = Math.random() * this.totalFitness;
        var theChosenOne: SGenome;

        var fitnessSoFar: number = 0;

        for (var i: number = 0; i<this.popSize; ++i) {
            fitnessSoFar += this.population[i].fitness;

            if (fitnessSoFar >= slice) {
                theChosenOne = this.population[i];
                break;
            }
        }

        return theChosenOne;
    }

    private crossover(mum: number[], dad: number[], baby1: number[], baby2: number[]): void {
        if (Math.random() > this.crossoverRate || mum == dad) {
			  for (var i = 0; i<mum.length; ++i) {
				  baby1.push(mum[i]);
				  baby2.push(dad[i]);
			  }
			  return;
        }

        var cp: number = parseInt((Math.random() * (this.chromoLength - 1)) + "");

        for (var i = 0; i<cp; ++i) {
            baby1.push(mum[i]);
            baby2.push(dad[i]);
        }

        for (i=cp; i<mum.length; ++i) {
            baby1.push(dad[i]);
            baby2.push(mum[i]);
        }
    }

    getChromos(): SGenome[] {
        return this.population;
    }

    epoch(oldPop: SGenome[]): SGenome[] {
        this.population = oldPop;

        this.reset();

        this.population.sort(function(lhs: SGenome, rhs: SGenome) {
            return lhs.fitness - rhs.fitness;
        });

        this.calculateBestWorstAvTot();

        var vecNewPop: SGenome[] = [];

        if (!(CParams.NUM_COPIES_ELITE * CParams.NUM_ELITE % 2)) {
            this.grabNBest(CParams.NUM_ELITE, CParams.NUM_COPIES_ELITE, vecNewPop);
        }

        while (vecNewPop.length < this.popSize) {
            var mum: SGenome = this.getChromoRoulette();
            var dad: SGenome = this.getChromoRoulette();

            var baby1: number[] = [];
            var baby2: number[] = [];

            this.crossover(mum.weights, dad.weights, baby1, baby2);

            this.mutate(baby1);
            this.mutate(baby2);

            vecNewPop.push(new SGenome(baby1, 0));
            vecNewPop.push(new SGenome(baby2, 0));
        }

        this.population = vecNewPop;

        return this.population;
    }

    private grabNBest(nBest: number, numCopies: number, pop: SGenome[]) {
        while(nBest--) {
            for (var i: number = 0; i<numCopies; ++i) {
                pop.push(this.population[(this.popSize - 1) - nBest]);
            }
        }
    }


    private calculateBestWorstAvTot(): void {
        this.totalFitness = 0;

        var highestSoFar: number = 0;
        var lowestSoFar: number = 9999999;

        for (var i: number = 0; i<this.popSize; ++i) {
            if (this.population[i].fitness > highestSoFar) {
                highestSoFar = this.population[i].fitness;
                this.fittestGenome = i;
                this.bestFitness = highestSoFar;
            }

            if (this.population[i].fitness < lowestSoFar) {
                lowestSoFar = this.population[i].fitness;
                this.worstFitness = lowestSoFar;
            }

            this.totalFitness += this.population[i].fitness;
        }

        this.averageFitness = this.totalFitness / this.popSize;
    }

    private reset(): void {
        this.totalFitness =    0;
        this.bestFitness =     0;
        this.worstFitness =    9999999;
        this.averageFitness =  0;
    }
}